#! /bin/bash
#

set -e

: ${MARIADB_PASSWORD:=password00}           # MariaDB's root password
: ${DRUPALDB_PASSWORD:=$MARIADB_PASSWORD}   # Drupal database password (user: drupal)
: ${DRUPAL_HOSTNAME:=$HOSTNAME}             # Name of drupal hostname
: ${APACHE_PORT:=80}                        # Apache listen to port
: ${DRUPAL_VERSION:=8.7.8}                  # Use Drupal version

# internal ip address
IP=$( ip -4 -o a sh dev eth0 | awk -F"[[:space:]/]+" "{ print \$4}" )

# Add proper record to hosts-file
if ! grep -q "^$IP" /etc/hosts; then
    sed -i -e "/$DRUPAL_HOSTNAME/d" /etc/hosts
    echo "$IP $DRUPAL_HOSTNAME" >> /etc/hosts
fi


# Upgrade system
dnf -y update
dnf -y upgrade

# Install and configure MariaDB
if ! systemctl list-units | grep -q mariadb; then
    dnf -y module install mariadb
else
    echo "MariaDB is already installed"
fi

if ! systemctl is-active --quiet mariadb; then
    systemctl enable --now mariadb

    # Harden MariaDB and set root password
    echo -e "\nY\n${MARIADB_PASSWORD}\n${MARIADB_PASSWORD}\nY\nY\nY\nY\n" | \
	    mysql_secure_installation
else
    echo "MariaDB is already ran"
fi

if ! mysql -uroot -p$MARIADB_PASSWORD --batch -e "SHOW DATABASES" | grep -q "^drupal"; then
    # Setup Drupal database
    cat << SQL | mysql -u root -p$MARIADB_PASSWORD
CREATE DATABASE drupal CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
CREATE USER drupal@localhost IDENTIFIED BY "$DRUPALDB_PASSWORD";
GRANT ALL PRIVILEGES ON drupal.* TO drupal@localhost IDENTIFIED BY "$DRUPALDB_PASSWORD";
FLUSH PRIVILEGES;
QUIT
SQL

    systemctl --no-pager --full status mariadb

else
    echo "DrupalDB is already created"
fi

# Install PHP and modules
if ! dnf --quiet list --installed php >/dev/null 2>&1; then
    dnf -y install @php
    dnf -y install php php-{cli,mysqlnd,json,opcache,xml,mbstring,gd,curl}
else
    echo "PHP is already installed"
fi

# Run PHP-FPM
if ! systemctl is-active --quiet php-fpm; then
    systemctl enable --now php-fpm

    systemctl --no-pager --full status php-fpm
else
    echo "PHP-FPM is already ran"
fi

# Install amd run Apache
if ! systemctl list-units | grep -q httpd; then
    dnf -y install @httpd
else
    echo "Apache HTTPD is already installed"
fi
if ! systemctl is-active --quiet httpd; then
    systemctl enable --now httpd
    systemctl --no-pager --full status httpd

    # Open ports for Apache
    firewall-cmd --add-service={http,https} --permanent --quiet
    if [ "x$APACHE_PORT" != "x" ]; then
        firewall-cmd --add-port=$APACHE_PORT/tcp --permanent --quiet
    fi
    firewall-cmd --reload --quiet

else
    echo "Apache HTTPD is already ran"
fi

# Install SElinux utilities
if ! dnf --quiet list --installed policycoreutils-python-utils >/dev/null 2>&1; then
    dnf -y install policycoreutils-python-utils
else
    echo "SElinux utils is already installed"
fi

# Download and install Drupal
if [ ! -d /var/www/html/drupal ]; then
    wget -qO- https://ftp.drupal.org/files/projects/drupal-${DRUPAL_VERSION}.tar.gz | \
        tar -xzf -
    mv -f drupal-${DRUPAL_VERSION} /var/www/html/drupal
    mkdir -p /var/www/html/drupal/sites/default/files
    cp -fZ /var/www/html/drupal/sites/default/default.settings.php \
	    /var/www/html/drupal/sites/default/settings.php

    # Set proper SElinux context for Drupal
    semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/drupal(/.*)?'
    semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/drupal/sites/default/settings.php'
    semanage fcontext -a -t httpd_sys_rw_content_t '/var/www/html/drupal/sites/default/files'
    restorecon -R /var/www/html/drupal
    restorecon    /var/www/html/drupal/sites/default/settings.php
    restorecon -R /var/www/html/drupal/sites/default/files
    chown -R apache:apache /var/www/html/drupal

    # Configure Apache to use Drupal
    cat << SH >| /etc/httpd/conf.d/drupal.conf
$( [[ "$APACHE_PORT" =~ (80|443) ]] || echo Listen $APACHE_PORT )
<VirtualHost *:$APACHE_PORT>
    ServerAdmin webmaster@${DRUPAL_HOSTNAME}
    ServerName ${DRUPAL_HOSTNAME}
    DocumentRoot /var/www/html/drupal
    <Directory /var/www/html/drupal/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
    ErrorLog /var/log/httpd/drupal_error.log
    CustomLog /var/log/httpd/drupal_access.log combined
</VirtualHost>
SH

    systemctl restart httpd
    systemctl --no-pager --full status httpd
else
    echo "Drupal is already installed"
fi

echo "Services:"
for i in mariadb php-fpm httpd; do
    echo $i $(systemctl is-active $i)
done

echo "Parameters:"
echo "MARIADB_PASSWORD:  $MARIADB_PASSWORD"
echo "DRUPALDB_PASSWORD: $DRUPALDB_PASSWORD"
echo "DRUPAL_VERSION:    $DRUPAL_VERSION"
echo "DRUPAL_HOSTNAME:   $DRUPAL_HOSTNAME"
echo "URL:               http://$IP:$APACHE_PORT"
echo "URL:               http://$DRUPAL_HOSTNAME:$APACHE_PORT"

exit $?

# vim: syn=bash ts=4 sw=4 smarttab expandtab
### That's all, folks!
