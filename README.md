# Exercise from course "Как стать системным администратором"

How to run:

```
vagrant up --provision --destroy-on-error 
```

There are two provision scripts -- one for Drupal and second
for Wordpress, by default execute both (dupal on port 80,
wordpress on port 81).

Also you can run provisioning separately, e.g.

```
vagrant up --provision --destroy-on-error --provision-with Wordpress
```
or

```
vagrant provision --provision-with Drupal
```

